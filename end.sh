#!usr/bin/env bash

# be sure to change both virtualenv directory and scrape/flightclub
# directory to where your venv and code is.
#source $WORKON_HOME/scrape/bin/activate
PATH=$PATH:/usr/local/bin
export PATH

NOW=$(date +"%F")
OUTPUT="end-$NOW.json"

cd /home/aaron/scrapers/end
scrapy crawl end -o /home/aaron/scraper_output/$OUTPUT
