#! -*- coding: utf-8 -*-

"""
Web Scraper Project

Scrape data from a regularly updated website endclothing.com and
save to a database (MySQL).

Scrapy pipeline part - stores scraped items in the database.
"""

import re

from sqlalchemy.orm import sessionmaker
from models import Products, Images, db_connect, create_products_table

from datetime import datetime, timedelta
from collections import OrderedDict

# import swaps.py from two directories up from pipelines.py
import sys
sys.path.insert(0, '/home/aaron/scrapers')
from swaps import Swaps


"""Collect filled publish slots:"""
publishengine = db_connect()
PublishSession = sessionmaker(bind=publishengine)
session = PublishSession()
db_scheduled = Swaps.db_scheduled
db_publish = session.execute(db_scheduled)
session.close()
scheduled = []
for row in db_publish:
    scheduled.append(str(row['publish']))
print " D A T A B A S E Publishing slots filled: "
print scheduled
queue = (datetime.now() + timedelta(hours=12)).replace(minute=Swaps.scheduler['end'], second=0, microsecond=0)
print "First publish time should be a slot just under 12 hours from now: " + queue.strftime("%Y-%m-%d %H:%M:%S")


class EndPipeline(object):
    """Pipeline for storing scraped items in the database"""
    def __init__(self):
        """
        Initializes database connection and sessionmaker.
        Creates Products table.
        """
        engine = db_connect()
        create_products_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        """
        Save Products in the database.
        This method is called for every item pipeline component.
        """

        item['releasedate'] = datetime.now().strftime("%m/%d/%Y")
        item['publish'] = self.time_increment(scheduled, queue)
        item['title'] = item['title'][0].title() 
        item['url'] = item['url'].replace('latest-products/', '').replace('latest-sneakers/', '')

        """
        Replacing:
        Punctuation and capitalization, roman numerals, 
        Followed by colors from nike, adidas, reebok
        """
        swap = OrderedDict(sorted(Swaps.mainswap.items()))
        # Removed model mainswap because model is set after title mainswap
        if item['title']:
            for category, swaps in swap.iteritems():
                swaps = OrderedDict(sorted(swaps.items()))
                if str(category).find('model') == -1:
                    for old, new in swaps.iteritems():
                        item['title'] = item['title'].replace(str(old),str(new))
            # Account for 'Womenss' because we need both 'Wmn' and 'Wmns' swaps:
            item['title'] = item['title'].replace('Womenss', 'Womens')
            if item['title'][:3] == 'es ':
                item['title'] = 'eS ' + item['title'][3:]
            if "Sneaker " in (item['title'] + " "):
                item['title'] = item['title'].replace(' Sneaker', '')
            item['model'] = item['title']
            if "Nike" in item['title'] or "Jordan" in item['title']:
                nikeclass = Swaps.nikeclass
                item['nikeclass'] = []
                for titleclass, store in nikeclass['storeclass'].iteritems():
                    if titleclass in item['title']:
                        item['nikeclass'].append(store)
                if len(item['nikeclass']) > 0:
                    item['nikeclass'] = ','.join(item['nikeclass'])
                else:
                    item['nikeclass'] = None
                for old, new in nikeclass['titlereplace'].iteritems():
                    if old in item['title']:
                        item['title'] = item['title'].replace(old,new)
                for old, new in nikeclass['modelreplace'].iteritems():
                    if old in item['model']:
                        item['model'] = item['model'].replace(old,new)             
            if 'Vans' in item['title'] and 'LX' in item['title']:
                if 'Vault' not in item['title']:
                    item['title'] = item['title'].replace('Vans', 'Vans Vault')
                item['title'] = item['title'].replace(' LX', '')
            # Set sizes field
            sizes = ['Baby', 'Kids', 'Preschool', 'Toddler', 'Womens']
            item['sizes'] = []
            for size in sizes:
                if size in item['title']:
                    item['sizes'].append(size)
            if len(item['sizes']) > 0:
                item['sizes'] = ','.join(item['sizes'])
            else:
                item['sizes'] = 'Mens'            
            #model-only formatting (already prepped in title; line 86)
            for old, new in swap['zzz_model'].iteritems():
                item['model'] = item['model'].replace(str(old),str(new))        
            if item['model'][:3] == 'es ':
                item['model'] = 'eS ' + item['model'][3:]
            tester = item['model'] + " "
            if " K " in tester or " M " in tester or " W " in tester:
                item['model'] = item['model'][:-2]
            elif " Ca " in tester or  " Pn " in tester or  " Pw " in tester:
                item['model'] = item['model'][:-3]
            elif " Br " in tester:
                item['model'] = item['model'][:-3] + "Breathe"
            if len(item['detail']) > 0:
                item['detail'] = item['detail'][0].replace('-', '/').replace(', ', '/').replace(' & ', '/')
            else:
                item['detail'] = None
            if item['detail']:
                item['title'] = item['title'] + " " + item['detail']
            if ' "' in item['model']:
                item['model'] = item['model'].split(' "')[0]
            if ' -' in item['model']:
                item['model'] = item['model'].split(' -')[0]
        # Format official brand colors
        if item['detail']:
            item['detail'] = item['detail'].title().replace('/', ',')
            for category, swaps in swap.iteritems():
                if 'color' in category:
                    for old, new in swaps.iteritems():
                        item['detail'] = item['detail'].replace(str(old),str(new))        
            brandcolors = Swaps.brandcolors
            for brand, brandcolor in brandcolors.iteritems():
                if brand in item['title'].lower():
                    item[brandcolor] = item['detail'].split('/')
                    for i in range(len(item[brandcolor])):
                        item[brandcolor][i] = item[brandcolor][i].strip()
                    item[brandcolor] = ','.join(item[brandcolor])        
        if item['price']:
            item['price'] = str(item['price'][0]).replace('$','').split('.')[0]
        else:
            item['price'] = None            
        # Image and Thumbnail
        if len(item['image']) > 0:
            item['image'] = self.image_order(self.magento_images(item['image']))
            item['thumbnail'] = item['image'][1]
        else:
            item['image'] = None
        # SKUs
        onedash = item['url'].rfind('-')
        twodash = item['url'].rfind('-', 0, onedash)
        threedash = item['url'].rfind('-', 0, twodash)
        fourdash = item['url'].rfind('-', 0, threedash)
        variablesku = {'two': {'eytys': 'eytyssku', 'undercover': 'undercoversku', 'shoes-like': 'shoeslikesku', 'superga': 'supergasku', 'zespa': 'zespasku'},
                       'three': {'apc': 'apcsku', 'jimmy-choo': 'jimmychoosku', 'neighborhood': 'neighborhoodsku', 'pierre-hardy': 'pierrehardysku', 'saturdays': 'saturdayssku', 'spalwart': 'spalwartsku'},
                       'four': {'etq': 'etqsku', 'moncler': 'monclersku'}}
        for brand, bsku in variablesku['two'].iteritems():
            if brand in item['url'] and 'nike' not in item['url']:
                item['sku'] = item['url'][twodash+1:-5]
        for brand, bsku in variablesku['three'].iteritems():
            if brand in item['url'] and 'adidas' not in item['url']:
                item['sku'] = item['url'][threedash+1:-5]
        for brand, bsku in variablesku['four'].iteritems():
            if brand in item['url']:
                item['sku'] = item['url'][fourdash+1:-5]
        if item['sku']:
            item['sku'] = item['sku'].upper()
            if "Nike" in item['title'] or "Jordan" in item['title']:
                item['nikestylecode'] = item['sku'].split('-',1)[0]
                item['nikecolorcode'] = item['sku'].split('-',1)[1]
        else:
            item['sku'] = None    
        if "balance" in item['url']:
            item['sku'] = item['url'][onedash+1:-5].upper()
            number_sku = re.search('\d', item['sku'])
            if number_sku: 
                first_num = number_sku.start()
                end_num = re.search('[A-z]', item['sku'][first_num:]).start() + first_num
                model_num = item['sku'][first_num:end_num]
                insensitive_title = re.compile(re.escape(item['sku']), re.IGNORECASE)
                item['title'] = insensitive_title.sub(model_num, item['title'])
                #item['title'].replace(item['sku'], model_num)
                item['model'] = insensitive_title.sub(model_num, item['model'])
                #item['model'].replace(item['sku'], model_num)
            if "England" in item['title']:
                item['origin'] = "United Kingdom"
                item['title'] = item['title'].replace(' Made In England','')
                item['model'] = item['model'].replace(' Made In England','')
            if "Usa" in item['title']:
                item['origin'] = "United States"
                item['title'] = item['title'].replace(' Made In The Usa','')
                item['model'] = item['model'].replace(' Made In The Usa','')

        
        """ Add item to database """
        
        #open session, create product_item for main titolo table
        session = self.Session()
        product_item = item.copy()
        del product_item['image']
        product = Products(**product_item)
        
        #commit product_item to main titolo table
        try:
            session.add(product)
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()
    
        #commit images to titolo_images table
        for order, image in item['image'].iteritems():
            image = Images(url=item['url'], image=image, imageorder=order)
            try:
                session.add(image)
                session.commit()
            except:
                session.rollback()
                raise
    
        return item
        
        
    """ Decide how often to publish these scraped items """
    def time_increment(self, scheduled, queue):
        timestring = queue.strftime("%Y-%m-%d %H:%M:%S")
        if timestring not in scheduled:
            queue += timedelta(hours=1)
            scheduled.append(timestring)
            return timestring
        else:
            queue += timedelta(hours=1)
            return self.time_increment(scheduled, queue)


    """ Rewrite urls to get original images from Magento: """        
    def magento_images(self, itemimage):
        for i in range(len(itemimage)):
            cache = itemimage[i].find('/cache')
            last_slash = itemimage[i].rfind('/', 0, itemimage[i].rfind('/', 0, itemimage[i].rfind('/')))
            itemimage[i] = itemimage[i][:cache] + itemimage[i][last_slash:]
        return itemimage


    """
    Transform list of image urls into ordered dictionary 
    (to allow MySQL sorting):
    """
    def image_order(self, itemimage):
        imageorder = {}
        if len(itemimage) > 0:
            for i in range(len(itemimage)):
                imageorder[i+1] = itemimage[i]
        return imageorder
