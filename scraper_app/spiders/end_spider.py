#! -*- coding: utf-8 -*-

"""
Web Scraper Project

Scrape data from a regularly updated website endclothing.com and
save to a database (MySQL).

Scrapy spider part - it actually performs scraping.
"""

from scrapy.spiders import Spider
from scrapy import Request, Selector

from scraper_app.models import db_connect
from scraper_app.items import EndItems

from sqlalchemy.orm import sessionmaker

from datetime import datetime

import cfscrape

import logging

logger = logging.getLogger('example')
logging.basicConfig(filename="end.log", level=logging.DEBUG)


class EndSpider(Spider):
    """
    Spider for regularly updated End Clothing 'Latest Sneakers' page
    """
    def __init__(self):
        """
        Initializes database connection and sessionmaker.
        """
        engine = db_connect()
        self.Session = sessionmaker(bind=engine)

    name = "end"
    allowed_domains = ["endclothing.com"]
    token, agent = cfscrape.get_tokens("http://www.endclothing.com/us/latest-products/latest-sneakers")

    def start_requests(self):
        yield Request( "http://www.endclothing.com/us/latest-products/latest-sneakers",
                      cookies=self.token,
                      headers={'User-Agent': self.agent},callback=self.first_parse)

    def first_parse(self, response):
        session = self.Session()
        dbquery = session.execute("SELECT url FROM end")
        scraped = []
        for row in dbquery:
            scraped.append(row['url'])
        session.close() 
        """
        next_url = response.xpath('//div[@class="pager"]//a[@title="Next"]/@href').extract()[0]
        print next_url
        yield Request( next_url,
                  cookies=self.token,
                  headers={'User-Agent': self.agent}, callback=self.next_parse)
        """
        for product in response.xpath('//a[contains(@class, "product-image")]/@href').extract():
            if product.replace('latest-products/', '').replace('latest-sneakers/', '') not in scraped:
                token, agent = cfscrape.get_tokens(product)
                yield Request(product,
                      cookies=token,
                      headers={'User-Agent': agent},
                      callback=self.parse_product)
            else:
                print product + " already scraped" 
    def next_parse(self, response):
        for product in response.xpath('//a[contains(@class, "product-image")]/@href').extract():
            yield Request(product, callback=self.parse_product)                  

    def parse_product(self, response):
        sel = Selector(response)
        item = EndItems()
        scrapedate = datetime.now()
        item['scrapedate'] = scrapedate.strftime("%Y-%m-%d %H:%M:%S")
        item['url'] = response.url
        item['title']  = sel.xpath('//h1/text()').extract()
        item['detail'] = sel.xpath('//*[@id="product_addtocart_form"]/div[2]/h3/text()').extract()
        item['price'] = sel.response.xpath('//span[@class="price"]/text()').extract()
        urlsku = {'acne': 10, 'adidas': 6, '/ami-': 13, 'asics': 10, 'buddy': 6, 'buttero': 8, 'camper': 11, 'comme-des': 8, 'common-projects': 9, 'converse': 7, 'diadora': 12, 'diemme': 10, 'filling-pieces': 11, 'fred-perry': 9, 'hender-scheme': 8, 'jordan': 10, 'lanvin': 12, 'margiela': 20, 'mcqueen': 17, 'nike': 10, 'no-288': 12, 'nonnative': 11, 'paul-smith': 13, 'puma': 8, 'reebok': 6, 'rick-owens': 13, 'saint-laurent': 18, 'saucony': 7, 'sophnet': 9, 'thom-browne': 17, 'valentino': 15, 'vans': 7, 'visvim': 16, 'wtaps': 7, 'y-3': 6, 'y3': 6, 'yeezy': 6, 'zanotti': 10}
        sku = ""
        for brand, skurange in urlsku.iteritems():
            if brand in item['url']:
                sku = item['url'][(-1*(skurange+5)):-5].upper()
        if 'balance' in item['url']:
            item['sku'] = item['url'][item['url'].rfind('-')+1:-5].upper()
        elif len(sku) > 0:
            item['sku'] = sku
        else:
            item['sku'] = sel.xpath('//ul/li[contains(.,"Style")]/text()').extract()
        item['image'] = sel.xpath('//*/img[contains(@src, "thumbnail/500")]/@src').extract()
        yield item
