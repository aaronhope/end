#! -*- coding: utf-8 -*-

"""
Web Scraper Project

Scrape data from a regularly updated website endclothing.com and
save to a database (MySQL).

Scrapy item part - defines container for scraped data.
"""

from scrapy.item import Item, Field


class EndItems(Item):
    """End container (dictionary-like object) for scraped data"""
    scrapedate = Field()
    publish = Field()
    releasedate = Field()
    
    url = Field()
    title = Field()
    detail = Field()
    price = Field()
    sku = Field()
    sizes = Field()
    
    image = Field()
    imageorder = Field()
    thumbnail = Field()
    
    nikestylecode = Field()
    nikecolorcode = Field()
    nikecolor = Field()
    nikeclass = Field()
    
    acnecolor = Field()
    adidascolor = Field()
    asicscolor = Field()
    conversecolor = Field()
    diadoracolor = Field()
    fredperrycolor = Field()
    margielacolor = Field()
    pumacolor = Field()
    reebokcolor = Field()
    sauconycolor = Field()
    vanscolor = Field()
    visvimcolor = Field()    
    
    acnesku = Field()
    adidassku = Field()
    amisku = Field()
    asicssku = Field()
    buddysku = Field()
    butterosku = Field()
    campersku = Field()
    commedessku = Field()
    commonprojectssku = Field()
    conversesku = Field()
    diadorasku = Field()
    diemmesku = Field()
    fillingpiecessku = Field()
    fredperrysku = Field()
    henderschemesku = Field()
    lanvinsku = Field()
    margielasku = Field()
    mcqueensku = Field()
    nbsku = Field()
    nonnativesku = Field()
    no288sku = Field()
    paulsmithsku = Field()
    pumasku = Field()
    reeboksku = Field()
    rickowenssku = Field()
    saintlaurentsku = Field()
    sauconysku = Field()
    sophnetsku = Field()
    thombrownesku = Field()
    vanssku = Field()
    valentinosku = Field()
    visvimsku = Field()
    wtapssku = Field()
    zanottisku = Field()
    
    model = Field()
    origin = Field()
