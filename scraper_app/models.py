#! -*- coding: utf-8 -*-

"""
Web Scraper Project

Scrape data from a regularly updated website endclothing.com and
save to a database (MySQL).

Database models part - defines table for storing scraped data.
Direct run will create the table.
"""

from sqlalchemy import create_engine, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL

import settings

# why was this here?
# if everything's fine without it, remove later
#from datetime import datetime


DeclarativeBase = declarative_base()


def db_connect():
    """Performs database connection using database settings from settings.py.

    Returns sqlalchemy engine instance.

    """
    return create_engine(URL(**settings.DATABASE))


def create_products_table(engine):
    """"""
    DeclarativeBase.metadata.create_all(engine)


class Products(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "end"

    scrapedate = Column('scrapedate', DateTime)
    publish = Column('publish', String(255), nullable=True)
    releasedate = Column('releasedate', String(255), nullable=True)
    url = Column('url', String(255), primary_key=True)
    title = Column('title', String(255))
    detail = Column('detail', String(255), nullable=True)
    price = Column('price', String(255), nullable=True)
    sku = Column('sku', String(255), nullable=True)
    sizes = Column('sizes', String(255), nullable=True)
    thumbnail = Column('thumbnail', String(255), nullable=True)
    nikecolor = Column('nikecolor', String(255), nullable=True)
    nikestylecode = Column('nikestylecode', String(6), nullable=True)
    nikecolorcode = Column('nikecolorcode', String(3), nullable=True)
    nikeclass = Column('nikeclass', String(255), nullable=True)
    adidascolor = Column('adidascolor', String(255), nullable=True)
    asicscolor = Column('asicscolor', String(255), nullable=True)
    reebokcolor = Column('reebokcolor', String(255), nullable=True)
    vanscolor = Column('vanscolor', String(255), nullable=True)
    acnesku = Column('acnesku', String(255), nullable=True)
    adidassku = Column('adidassku', String(255), nullable=True)
    adidascolor = Column('adidascolor', String(255), nullable=True)
    amisku = Column('amisku', String(255), nullable=True)
    asicssku = Column('asicssku', String(255), nullable=True)
    asicscolor = Column('asicscolor', String(255), nullable=True)
    buddysku = Column('buddysku', String(255), nullable=True)
    butterosku = Column('butterosku', String(255), nullable=True)
    campersku = Column('campersku', String(255), nullable=True)
    commedessku = Column('commedessku', String(255), nullable=True)
    commonprojectssku = Column('commonprojectssku', String(255), nullable=True)
    conversesku = Column('conversesku', String(255), nullable=True)
    conversecolor = Column('conversecolor', String(255), nullable=True)
    diadorasku = Column('diadorasku', String(255), nullable=True)
    diadoracolor = Column('diadoracolor', String(255), nullable=True)
    diemmesku = Column('diemmesku', String(255), nullable=True)
    fredperrysku = Column('fredperrysku', String(255), nullable=True)
    fredperrycolor = Column('fredperrycolor', String(255), nullable=True)
    fillingpiecessku = Column('fillingpiecessku', String(255), nullable=True)
    henderschemesku = Column('henderschemesku', String(255), nullable=True)
    lanvinsku = Column('lanvinsku', String(255), nullable=True)
    margielasku = Column('margielasku', String(255), nullable=True)
    margielacolor = Column('margielacolor', String(255), nullable=True)
    mcqueensku = Column('mcqueensku', String(255), nullable=True)    
    nbsku = Column('nbsku', String(255), nullable=True)
    nonnativesku = Column('nonnativesku', String(255), nullable=True)
    no288sku = Column('no288sku', String(255), nullable=True)
    paulsmithsku = Column('paulsmithsku', String(255), nullable=True)
    pumasku = Column('pumasku', String(255), nullable=True)
    pumacolor = Column('pumacolor', String(255), nullable=True)
    reeboksku = Column('reeboksku', String(255), nullable=True)
    reebokcolor = Column('reebokcolor', String(255), nullable=True)
    rickowenssku = Column('rickowenssku', String(255), nullable=True)
    saintlaurentsku = Column('saintlaurentsku', String(255), nullable=True)
    sauconysku = Column('sauconysku', String(255), nullable=True)
    sauconycolor = Column('sauconycolor', String(255), nullable=True)
    sophnetsku = Column('sophnetsku', String(255), nullable=True)
    thombrownesku = Column('thombrownesku', String(255), nullable=True)
    vanssku = Column('vanssku', String(255), nullable=True)
    vanscolor = Column('vanscolor', String(255), nullable=True)
    valentinosku = Column('valentinosku', String(255), nullable=True)
    visvimsku = Column('visvimsku', String(255), nullable=True)
    visvimcolor = Column('visvimcolor', String(255), nullable=True)
    wtapssku = Column('wtapssku', String(255), nullable=True)
    zanottisku = Column('zanottisku', String(255), nullable=True)
    model = Column('model', String(255), nullable=True)
    origin = Column('origin', String(255), nullable=True)
    

class Images(DeclarativeBase):
    """Sqlalchemy deals model"""
    __tablename__ = "end_images"

    url = Column('url', String(255), ForeignKey('end.url'))
    image = Column('image', String(255), primary_key=True)
    imageorder = Column('imageorder', Integer)
