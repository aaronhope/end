# Scrapy settings for tutorial project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

""" Importing from passwords.py, which is listed in .gitignore """
# import passwords.py from two directories up from pipelines.py
import sys
sys.path.insert(0, '/home/aaron/scrapers')
from passwords import dbpassword 


BOT_NAME = 'end'

SPIDER_MODULES = ['scraper_app.spiders']

ITEM_PIPELINES = {'scraper_app.pipelines.EndPipeline': 100}

DATABASE = dbpassword

DUPEFILTER_DEBUG = True
CONCURRENT_REQUESTS = 1
